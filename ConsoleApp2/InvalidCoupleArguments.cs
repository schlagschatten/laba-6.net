using System;

namespace ConsoleApp2
{
    internal class InvalidCoupleArguments : Exception
    {
        #region Constructors

        public InvalidCoupleArguments(string message) : base(message)
        {
        }

        #endregion Constructors
    }
}