namespace ConsoleApp2
{
    public interface IHasName
    {
        #region Properties

        string Name { get; }

        #endregion Properties
    }
}